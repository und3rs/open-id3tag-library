#include <iostream>
#include <string>
#include "ID3Tag.h"
#include <fstream>

int main(int argc, char * argv[]) {
	if (argc < 2) {
		wcout << "Usage: ID3C.exe [Media File Path]" << endl;
		return 0;
	}

	MetadataInfo::ID3::ID3Tag Meta;
	if (Meta.Open(argv[1]) == true) {
		wcout << "TITLE: " << Meta.GetStringValue(MetadataInfo::ID3::TITLE) << endl;
		wcout << "ALBUM: " << Meta.GetStringValue(MetadataInfo::ID3::ALBUM) << endl;
		wcout << "ARTIST: " << Meta.GetStringValue(MetadataInfo::ID3::ARTIST) << endl;
		wcout << "ARTIST_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::ARTIST_WEB) << endl;
		wcout << "AUDIO_ENCRYPT: " << Meta.GetStringValue(MetadataInfo::ID3::AUDIO_ENCRYPT) << endl;
		wcout << "BAND: " << Meta.GetStringValue(MetadataInfo::ID3::BAND) << endl;
		wcout << "BPM: " << Meta.GetStringValue(MetadataInfo::ID3::BPM) << endl;
		wcout << "TRACK: " << Meta.GetStringValue(MetadataInfo::ID3::TRACK) << endl;
		wcout << "CD_ID: " << Meta.GetStringValue(MetadataInfo::ID3::CD_ID) << endl;
		wcout << "COMMENT: " << Meta.GetStringValue(MetadataInfo::ID3::COMMENT) << endl;
		wcout << "COMMERCIAL_FRAME: " << Meta.GetStringValue(MetadataInfo::ID3::COMMERCIAL_FRAME) << endl;
		wcout << "COMMERCIAL_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::COMMERCIAL_WEB) << endl;
		wcout << "COMPOSERS: " << Meta.GetStringValue(MetadataInfo::ID3::COMPOSERS) << endl;
		wcout << "CONDUCTOR: " << Meta.GetStringValue(MetadataInfo::ID3::CONDUCTOR) << endl;
		wcout << "CONTENT_GROUP: " << Meta.GetStringValue(MetadataInfo::ID3::CONTENT_GROUP) << endl;
		wcout << "CONTENT_TYPE: " << Meta.GetStringValue(MetadataInfo::ID3::CONTENT_TYPE) << endl;
		wcout << "COPYRIGHT: " << Meta.GetStringValue(MetadataInfo::ID3::COPYRIGHT) << endl;
		wcout << "COPYRIGHT_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::COPYRIGHT_WEB) << endl;
		wcout << "DATE: " << Meta.GetStringValue(MetadataInfo::ID3::DATE) << endl;
		wcout << "ENCODER: " << Meta.GetStringValue(MetadataInfo::ID3::ENCODER) << endl;
		wcout << "ENCODING_SETTING: " << Meta.GetStringValue(MetadataInfo::ID3::ENCODING_SETTING) << endl;
		wcout << "ENCRYPT_METHOD: " << Meta.GetStringValue(MetadataInfo::ID3::ENCRYPT_METHOD) << endl;
		wcout << "END_TIME: " << Meta.GetStringValue(MetadataInfo::ID3::END_TIME) << endl;
		wcout << "EQUALIZATION: " << Meta.GetStringValue(MetadataInfo::ID3::EQUALIZATION) << endl;
		wcout << "EVENT_CODES: " << Meta.GetStringValue(MetadataInfo::ID3::EVENT_CODES) << endl;
		wcout << "FILE_OWNER: " << Meta.GetStringValue(MetadataInfo::ID3::FILE_OWNER) << endl;
		wcout << "FILE_TYPE: " << Meta.GetStringValue(MetadataInfo::ID3::FILE_TYPE) << endl;
		wcout << "FILE_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::FILE_WEB) << endl;
		wcout << "GENERAL_ENCAPSULATED_OBJ: " << Meta.GetStringValue(MetadataInfo::ID3::GENERAL_ENCAPSULATED_OBJ) << endl;
		wcout << "GENRE: " << Meta.GetStringValue(MetadataInfo::ID3::GENRE) << endl;
		wcout << "GROUP_ID: " << Meta.GetStringValue(MetadataInfo::ID3::GROUP_ID) << endl;
		wcout << "INITIAL_KEY: " << Meta.GetStringValue(MetadataInfo::ID3::INITIAL_KEY) << endl;
		wcout << "INTERNET_RADIO_STATION_NAME: " << Meta.GetStringValue(MetadataInfo::ID3::INTERNET_RADIO_STATION_NAME) << endl;
		wcout << "INTERNET_RADIO_STATION_OWNER: " << Meta.GetStringValue(MetadataInfo::ID3::INTERNET_RADIO_STATION_OWNER) << endl;
		wcout << "INTERNET_RADIO_STATION_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::INTERNET_RADIO_STATION_WEB) << endl;
		wcout << "INVOLVED_PEOPLES: " << Meta.GetStringValue(MetadataInfo::ID3::INVOLVED_PEOPLES) << endl;
		wcout << "ITNL_STD_RCD_CDE: " << Meta.GetStringValue(MetadataInfo::ID3::ITNL_STD_RCD_CDE) << endl;
		wcout << "LANGUAGE: " << Meta.GetStringValue(MetadataInfo::ID3::LANGUAGE) << endl;
		wcout << "LENGTH: " << Meta.GetStringValue(MetadataInfo::ID3::LENGTH) << endl;
		wcout << "LINK_INFO: " << Meta.GetStringValue(MetadataInfo::ID3::LINK_INFO) << endl;
		wcout << "LOCATION_TABLE: " << Meta.GetStringValue(MetadataInfo::ID3::LOCATION_TABLE) << endl;
		wcout << "LYRIC: " << Meta.GetStringValue(MetadataInfo::ID3::LYRIC) << endl;
		wcout << "LYRICIST: " << Meta.GetStringValue(MetadataInfo::ID3::LYRICIST) << endl;
		wcout << "MEDIA_TYPE: " << Meta.GetStringValue(MetadataInfo::ID3::MEDIA_TYPE) << endl;
		wcout << "MODIFIER: " << Meta.GetStringValue(MetadataInfo::ID3::MODIFIER) << endl;
		wcout << "NONE: " << Meta.GetStringValue(MetadataInfo::ID3::NONE) << endl;
		wcout << "ORIGIN_ALBUM: " << Meta.GetStringValue(MetadataInfo::ID3::ORIGIN_ALBUM) << endl;
		wcout << "ORIGIN_ARTIST: " << Meta.GetStringValue(MetadataInfo::ID3::ORIGIN_ARTIST) << endl;
		wcout << "ORIGIN_TITLE: " << Meta.GetStringValue(MetadataInfo::ID3::ORIGIN_TITLE) << endl;
		wcout << "ORIGIN_LYRICIST: " << Meta.GetStringValue(MetadataInfo::ID3::ORIGIN_LYRICIST) << endl;
		wcout << "ORIGIN_YEAR: " << Meta.GetStringValue(MetadataInfo::ID3::ORIGIN_YEAR) << endl;
		wcout << "OWNERSHIP_FRAME: " << Meta.GetStringValue(MetadataInfo::ID3::OWNERSHIP_FRAME) << endl;
		wcout << "PART: " << Meta.GetStringValue(MetadataInfo::ID3::PART) << endl;
		wcout << "PAYMENT_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::PAYMENT_WEB) << endl;
		wcout << "PLAYLIST_DELAY: " << Meta.GetStringValue(MetadataInfo::ID3::PLAYLIST_DELAY) << endl;
		wcout << "PLAY_COUNTER: " << Meta.GetStringValue(MetadataInfo::ID3::PLAY_COUNTER) << endl;
		wcout << "POPULARIMETER: " << Meta.GetStringValue(MetadataInfo::ID3::POPULARIMETER) << endl;
		wcout << "POSITION_SYNCHRONISATION_FRAME: " << Meta.GetStringValue(MetadataInfo::ID3::POSITION_SYNCHRONISATION_FRAME) << endl;
		wcout << "PRIVATE_FRAME: " << Meta.GetStringValue(MetadataInfo::ID3::PRIVATE_FRAME) << endl;
		wcout << "PUBLISHER: " << Meta.GetStringValue(MetadataInfo::ID3::PUBLISHER) << endl;
		wcout << "PUBLISHER_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::PUBLISHER_WEB) << endl;
		wcout << "RECOMMENDED_BUFFER_SIZE: " << Meta.GetStringValue(MetadataInfo::ID3::RECOMMENDED_BUFFER_SIZE) << endl;
		wcout << "RECORDING_DATE: " << Meta.GetStringValue(MetadataInfo::ID3::RECORDING_DATE) << endl;
		wcout << "RELATIVE_VOLUME_ADJUSTMENT: " << Meta.GetStringValue(MetadataInfo::ID3::RELATIVE_VOLUME_ADJUSTMENT) << endl;
		wcout << "REVERBB: " << Meta.GetStringValue(MetadataInfo::ID3::REVERBB) << endl;
		wcout << "SIZE: " << Meta.GetStringValue(MetadataInfo::ID3::SIZE) << endl;
		wcout << "SOURCE_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::SOURCE_WEB) << endl;
		wcout << "SPEED: " << Meta.GetStringValue(MetadataInfo::ID3::SPEED) << endl;
		wcout << "START_TIME: " << Meta.GetStringValue(MetadataInfo::ID3::START_TIME) << endl;
		wcout << "SUBTITLE: " << Meta.GetStringValue(MetadataInfo::ID3::SUBTITLE) << endl;
		wcout << "SYNCH_LYRIC: " << Meta.GetStringValue(MetadataInfo::ID3::SYNCH_LYRIC) << endl;
		wcout << "SYNCH_TEMPO_CODES: " << Meta.GetStringValue(MetadataInfo::ID3::SYNCH_TEMPO_CODES) << endl;
		wcout << "TERMS_OF_USER: " << Meta.GetStringValue(MetadataInfo::ID3::TERMS_OF_USER) << endl;
		wcout << "TIME: " << Meta.GetStringValue(MetadataInfo::ID3::TIME) << endl;
		wcout << "TRACK: " << Meta.GetStringValue(MetadataInfo::ID3::TRACK) << endl;
		wcout << "UNIQUE_FILE_ID: " << Meta.GetStringValue(MetadataInfo::ID3::UNIQUE_FILE_ID) << endl;
		wcout << "USER_DEFINED_WEB: " << Meta.GetStringValue(MetadataInfo::ID3::USER_DEFINED_WEB) << endl;
		wcout << "YEAR: "			<< Meta.GetStringValue(MetadataInfo::ID3::YEAR) << endl;
		
		// extract binary data - like cover image.

		long size = Meta.GetBinarySize(MetadataInfo::ID3::COVER_IMAGE);
		char * coverImage = (char*)malloc(size);
		memset(coverImage, 0, size);
		if (Meta.GetBinaryValue(MetadataInfo::ID3::COVER_IMAGE, coverImage, size) > 0) {
			long imageDataSize = 0;
			const char * imageData = Meta.GetBinaryImage(coverImage, size, imageDataSize);

			char * buffer = (char *)malloc(strlen(argv[1]) + strlen(".jpg"));
			strcpy(buffer, argv[1]);
			strcat(buffer, ".jpg");

			ofstream SaveFile(buffer, ios_base::out | ios_base::binary);
			SaveFile.write(imageData, imageDataSize);
			SaveFile.close();

			cout << "COVER_IMAGE: (saved in '" << buffer << "')" << endl;
		}
		delete coverImage;

		//
		wcout << "USER_DEFINED: " << Meta.GetStringValue(MetadataInfo::ID3::USER_DEFINED) << endl;
	}
	Meta.Close();
	return 0;
}